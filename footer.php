<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

?>

<?php if (is_front_page()) { ?>
    </div>
<?php } ?>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- <div class="navbar-header">
                    <a href="#">Jakz Aizzat</a>
                  </div> -->
                <div class="footer-social">
                    <div class="row">
                        <div class="col">
                            <a href="https://twitter.com/jakzaizzat" target="_blank">
                          <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        </div>
                        <div class="col">
                            <a href="https://instagram.com/epalvalley" target="_blank">
                          <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                        </div>
                        <div class="col">
                            <a href="https://github.com/jakzaizzat" target="_blank">
                          <i class="fa fa-github" aria-hidden="true"></i>
                        </a>
                        </div>
                        <div class="col">
                            <a href="https://www.linkedin.com/in/mohd-aizzat-%E2%9A%A1%EF%B8%8F-03810791/" target="_blank">
                          <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                        </div>
                        <div class="col">
                            <a href="http://epalvalley.wasap.my/" target="_blank">
                          <i class="fa fa-whatsapp" aria-hidden="true"></i>
                        </a>
                        </div>
                    </div>
                </div>
                <div class="footer-text">
                    <p>Made with ☕️ from 🇲🇾</p>
                    <p>© 2018 Jakz Aizzat</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106536425-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-106536425-1');
</script>

<?php wp_footer(); ?>
<script>
			AOS.init({
				easing: 'fade-left',
				duration: 1000
			});
  </script>
</body>

</html>

