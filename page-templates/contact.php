<?php
/**
 * Template Name: Contact Template
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

get_header(); ?>

<section class="contact-page">
	<div class="container">
	    <h1 class="heading text-center">Contact</h1>
        <div class="card">
        <?php echo do_shortcode('[contact-form-7 id="27" title="Contact"]'); ?>
        </div>
	</div>
</section>

<?php get_footer(); ?>
