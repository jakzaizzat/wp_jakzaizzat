<?php
/**
 * Template Name: Why Me Template
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header(); ?>

<section>
	<div class="container">
	    <h1 class="heading text-center">Why Work with me?</h1>
	</div>
</section>

<section class="me-pages overflow-hidden">
    <p>Let’s get down to business.</p>
    <p>However we gotta make sure we mash well together. I’ve been freelancing and working as a front-end developer for over 2 years now, so I have experience. I know exactly how it feels to put your money into something and someone you don’t know yet. And not knowing if it will work out. It’s scary right? Lemme just say you’re not alone. I’m just as nervous about putting 100% of my time and effort in. We’re both in the same boat. Because let’s be honest, we’re unsure of each other because we just met.</p>
    <p>So don’t worry. Here’s the reasons why I’m the suitable developer for your project.</p>
    <div class="row reason" data-aos="fade-left" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-sm-2">
            <h2>No need to deal with big agency</h2>
            <p>You're only working with me. I work super fast. Have a question? E-mail me and I will answer it in 24 hours.</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-sm-1">
            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/icon/personal.svg"/>
        </div>
    </div>
    <div class="row reason" data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-2 order-sm-2">
            <h2>Focus on business goal</h2>
            <p>Website without generating money is bullshit. The reasons you are asking me to build your website is to make you rich. We will brainstorming together and find a way to make you success.</p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-1 order-sm-1">
            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/icon/increase-revenue.svg"/>
        </div>
    </div>
    <div class="row reason" data-aos="fade-left" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-sm-2">
            <h2>I report to you weekly</h2>
            <p>You're my workplace. I will send you a weekly report what I'm done and my progress.I will give you an access to my <a href="https://trello.com/">Trello</a>, you may see what I'm working on. </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 order-sm-1">
            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/img/icon/report.svg"/>
        </div>
    </div>
</section>

<?php get_footer(); ?>
