<?php
/**
 * Template Name: Services
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package understrap
 */

get_header(); ?>

<section>
	<div class="container">
	<h1 class="heading text-center">What I can help you</h1>
	</div>
</sectio>

<section class="services-page">
<div class="single-service overflow-hidden" id="website">
			<div class="container">
				<h2 class="subheading text-center">Website Development</h2>
				<p class="subheading-desc">You will get a website to reach your business goal. </p>
				<div class="row overflow-hidden">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" data-aos="fade-right" data-aos-offset="0" data-aos-easing="ease-in-sine" data-aos-duration="600">
						<div class="card">
						<img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/icon/increase-revenue.svg" alt="Card image cap">
						<div class="card-body">
							<h4 class="card-title">Increase Revenue</h4>
							<p class="card-text">Your sales is quite down this month? Let's brainstorming together and comes up with proper tactics to increase the conversions.</p>
						</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" data-aos="fade-left" data-aos-offset="0" data-aos-easing="ease-in-sine" data-aos-duration="600">
						<div class="card">
						<img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/icon/reduce-cost.svg" alt="Card image cap">
						<div class="card-body">
							<h4 class="card-title">Reduce Cost</h4>
							<p class="card-text">Do you have an extra staff to handle the inventory? Reduce the cost with inventory system and you can fired one of you staff. </p>
						</div>
						</div>
					</div>
				</div>
				<div class="services-explanation">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h3 class="subsection">This is what you get</h3>
							<p class="subsection-desc">Full package with business driven.</p>
							<ul class="inline-list">
								<li>
									<i class="fa fa-circle"></i>Responsive Design
								</li>
								<li><i class="fa fa-circle"></i>Custom WordPress Development</li>
								<li><i class="fa fa-circle"></i>Focus on result. Not a stupid website</li>
								<li><i class="fa fa-circle"></i>Friendly Admin Dashboard</li>
								<li><i class="fa fa-circle"></i>Align with your marketing strategy</li>
								<li><i class="fa fa-circle"></i>Make you rich 🤑</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="service-price" data-aos="fade-up" data-aos-offset="0" data-aos-easing="ease-in-sine" data-aos-duration="600">
				<p>👨🏻‍💻 Website Development starts at <br><span>RM 1500 - RM 2500</span><p>
			</div>
		</div>
		<div class="single-service overflow-hidden" id="maintenance">
			<div class="container">
				<h2 class="subheading text-center">Maintenance</h2>
				<p class="subheading-desc">The support your website needs to maintain and increase its effectiveness.</p>
				<div class="row overflow-hidden">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" data-aos="fade-left" data-aos-offset="0" data-aos-easing="ease-in-sine" data-aos-duration="600">
						<div class="card">
						<img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/icon/security.svg" alt="Card image cap">
						<div class="card-body">
							<h4 class="card-title">Security</h4>
							<p class="card-text">Your website will be backup <strong>everyday</strong> and if there's a down time, I straight out of bed and fix it 👷🏻</p>
						</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" data-aos="fade-right" data-aos-offset="0" data-aos-easing="ease-in-sine" data-aos-duration="600">
						<div class="card">
						<img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/icon/report.svg" alt="Card image cap">
						<div class="card-body">
							<h4 class="card-title">Report</h4>
							<p class="card-text">You will get a monthly report what's happen to your website and which marketing channels are working well.</p>
						</div>
						</div>
					</div>
				</div>
				<div class="services-explanation">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h3 class="subsection">This is what you get</h3>
							<p class="subsection-desc">Leave me handle your website, you handle your business</p>
							<ul class="inline-list">
								<li>
									<i class="fa fa-circle"></i>Daily Backup
								</li>
								<li><i class="fa fa-circle"></i>WordPress Updates</li>
								<li><i class="fa fa-circle"></i>Website Suggestion Monthly</li>
								<li><i class="fa fa-circle"></i>24/7/365 Security Monitoring</li>
								<li><i class="fa fa-circle"></i>Monthly report traffic</li>
								<li><i class="fa fa-circle"></i>Direct personal access to me</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="service-price" data-aos="fade-up" data-aos-offset="0" data-aos-easing="ease-in-sine" data-aos-duration="600">
				<p>💵 Maintenace services starts at <br><span>RM150/month</span><p>
			</div>
		</div>
</section>

<?php get_footer();