<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>
<!-- 
<article class="article" <?php post_class(); ?> id="post-<?php the_ID(); ?>"> -->

	<?php
		$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
	 ?>
	 
	<a href="<?= get_permalink() ?>" class="articles" <?php post_class(); ?> id="post-<?php the_ID(); ?>" style="background-image: url(' <?= $featured_img_url; ?> ');">
		<div class="articles__meta">
			<h1><?= get_the_title(); ?></h1>
			<p><?= the_excerpt(); ?></p>
			<span class="articles__date">
				<?php echo human_time_diff( get_the_date('U'), current_time('timestamp') ) . ' ago'; ?>
	    	</span>
		</div>
	</a>

<!-- </article> -->
