<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>


	<?php
		$featured_img_url = get_the_post_thumbnail_url( $post->ID, "full" );
	 ?>

<div class="single-blog-featured" style="background-image: url(' <?= $featured_img_url; ?> ');">

</div>

<div class="single-blog-container">
<header class="single-blog-title">
		<?php the_title( '<h1>', '</h1>' ); ?>
    <hr>
    <span class="date">
		<?php echo human_time_diff( get_the_date('U'), current_time('timestamp') ) . ' ago'; ?>
	</span>
</header>

<article class="single-blog-content" <?php post_class(); ?> id="post-<?php the_ID(); ?>">


		<?php the_content(); ?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
			'after'  => '</div>',
		) );
		?>
	<div class="single-blog-social">
		<a href="https://twitter.com/intent/tweet?url=<?php echo get_permalink(); ?>&text=<?php echo the_title(); ?> by @jakzaizzat" class="tweet-share btn" target="_blank">
			<svg xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300.00006 244.18703" height="244.19" width="300" version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/">
				<g style="" transform="translate(-539.18 -568.86)">
					<path d="m633.9 812.04c112.46 0 173.96-93.168 173.96-173.96 0-2.6463-0.0539-5.2806-0.1726-7.903 11.938-8.6302 22.314-19.4 30.498-31.66-10.955 4.8694-22.744 8.1474-35.111 9.6255 12.623-7.5693 22.314-19.543 26.886-33.817-11.813 7.0031-24.895 12.093-38.824 14.841-11.157-11.884-27.041-19.317-44.629-19.317-33.764 0-61.144 27.381-61.144 61.132 0 4.7978 0.5364 9.4646 1.5854 13.941-50.815-2.5569-95.874-26.886-126.03-63.88-5.2508 9.0354-8.2785 19.531-8.2785 30.73 0 21.212 10.794 39.938 27.208 50.893-10.031-0.30992-19.454-3.0635-27.69-7.6468-0.009 0.25652-0.009 0.50661-0.009 0.78077 0 29.61 21.075 54.332 49.051 59.934-5.1376 1.4006-10.543 2.1516-16.122 2.1516-3.9336 0-7.766-0.38716-11.491-1.1026 7.7838 24.293 30.355 41.971 57.115 42.465-20.926 16.402-47.287 26.171-75.937 26.171-4.929 0-9.7983-0.28036-14.584-0.84634 27.059 17.344 59.189 27.464 93.722 27.464" fill="#1da1f2"/>
				</g>
			</svg>
			<span>Tweet</span>
		</a>
		<a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" class="tweet-share btn" target="_blank">
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="266.893px" height="266.895px" viewBox="0 0 266.893 266.895" enable-background="new 0 0 266.893 266.895"
				 xml:space="preserve">
			<path id="Blue_1_" fill="#3C5A99" d="M248.082,262.307c7.854,0,14.223-6.369,14.223-14.225V18.812
				c0-7.857-6.368-14.224-14.223-14.224H18.812c-7.857,0-14.224,6.367-14.224,14.224v229.27c0,7.855,6.366,14.225,14.224,14.225
				H248.082z"/>
			<path id="f" fill="#FFFFFF" d="M182.409,262.307v-99.803h33.499l5.016-38.895h-38.515V98.777c0-11.261,3.127-18.935,19.275-18.935
				l20.596-0.009V45.045c-3.562-0.474-15.788-1.533-30.012-1.533c-29.695,0-50.025,18.126-50.025,51.413v28.684h-33.585v38.895h33.585
				v99.803H182.409z"/>
			</svg>

			<span>Share</span>
		</a>
	</div>
</article><!-- #post-## -->

</div>